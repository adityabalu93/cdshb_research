from models import load_model
from torch.autograd import Variable
from optimizers.MSGD import MSGD
from optimizers.CDSGD import CDSGD
from optimizers.NCDSGD import NCDSGD
from optimizers.CDMSGD1 import CDMSGD1
from optimizers.CDMSGD2 import CDMSGD2
from optimizers.ICDSGD import ICDSGD
from optimizers.ICDMSGD import ICDMSGD
from optimizers.GCDSGD import GCDSGD
from optimizers.GCDMSGD import GCDMSGD
from optimizers.DMSGD1 import DMSGD1
from optimizers.DMSGD2 import DMSGD2
from optimizers.CDADAM import CDADAM
from optimizers.CDADAMSGD import CDADAMSGD
from optimizers.ICDADAM import ICDADAM
from optimizers.GCDADAM import GCDADAM
from optimizers.FedAvg import FedAvg
from optimizers.CGD import CGD
from optimizers.MCGD import MCGD
from torch.optim import SGD#, Adadelta, Adagrad, Adam, Adamax, ASGD, LBFGS
import torch.utils.data as data
import numpy as np
import json
import os
from copy import deepcopy
import torch
import torch.backends.cudnn as cudnn
from collections import defaultdict

opt_list = {'SGD': SGD, 'MSGD': MSGD, 'NCDSGD': NCDSGD, 'CDSGD': CDSGD, 'CDMSGD1': CDMSGD1, 'CDMSGD2': CDMSGD2, 'ICDMSGD':ICDMSGD, 
			'ICDSGD':ICDSGD, 'GCDSGD':GCDSGD, 'GCDMSGD':GCDMSGD, 'DMSGD1':DMSGD1, 'DMSGD2':DMSGD2, 'CDADAM':CDADAM, 'CDADAMSGD':CDADAMSGD,
			'ICDADAM':ICDADAM,'GCDADAM':GCDADAM, 'FedAvg':FedAvg, 'CGD':CGD, 'MCGD':MCGD}


class Agent(object):
	"""docstring for  Agent for distributed deep learning"""
	def __init__(self, train_data, test_data, nb_classes, agent_id=1, **kwargs):
		self.agent_id = agent_id
		self.train_data_loader = data.DataLoader(train_data, kwargs['batch_size'], shuffle=True)
		self.test_data_loader = data.DataLoader(test_data, kwargs['test_batch_size'], shuffle=True)
		self.model_name = kwargs['model_name']
		self.experiment_name = kwargs ['experiment_name']
		self.dataset = kwargs ['dataset']
		img_dim = train_data.train_data.shape[0:] # single_agent: (50000, 3, 32, 32), 5 agents: (10000, 3, 32, 32)

		if self.dataset == 'mnist' or self.dataset == 'semeion':
			ch_dim = img_dim[1]
		else:
			ch_dim = 3

		# Setting fc_nodes for CNN:
		if self.model_name == 'CNN':
			if self.dataset == 'mnist':
				fc_nodes = 3136
			elif self.dataset == 'stl10':
				fc_nodes = 36864
			elif self.dataset == 'cifar10' or self.dataset == 'cifar100':
				fc_nodes = 4096

		if self.model_name == 'CNN2':
			if self.dataset == 'mnist':
				fc_nodes = 576
			elif self.dataset == 'stl10':
				fc_nodes = 9216
			elif self.dataset == 'cifar10' or self.dataset == 'cifar100':
				fc_nodes = 1024

		if self.model_name == 'LR':
			input_dim = img_dim[1]*img_dim[2]*img_dim[3]
			fc_nodes = None
			model, self.criterion = load_model(self.model_name, input_dim, fc_nodes, nb_classes)
		else:
			model, self.criterion = load_model(self.model_name, ch_dim, fc_nodes, nb_classes)

		self.model = torch.nn.DataParallel(model).cuda()
		cudnn.benchmark = True
		
		if kwargs['verbose'] >= 2:
			print('Total params: %.2fM' % (sum(p.numel() for p in model.parameters())/1000000.0))
			print('learning_rate:',kwargs['learning_rate'],'momentum:',kwargs['momentum'])
		
		try:
			self.optimizer = opt_list[kwargs['experiment_name']](
				self.model.parameters(), lr=kwargs['learning_rate'],
				momentum=kwargs['momentum'], omega=kwargs['omega'])
		except:
			self.optimizer = opt_list[kwargs['experiment_name']](
				self.model.parameters(), lr=kwargs['learning_rate'],
				momentum=kwargs['momentum'])
		
		self.kwargs = kwargs
		self.train_loss_hist = []
		self.test_loss_hist = []
		self.global_train_loss_hist = []
		self.acc_hist = []


	def train(self, agent_id):
		self.model.train()
		total_train_loss = 0.
		
		for batch_idx, (inputs, target) in enumerate(self.train_data_loader):
			if self.kwargs['cuda']:
				inputs, target = inputs.cuda(), target.cuda()
			inputs, target = Variable(inputs), Variable(target) # ori
			self.optimizer.zero_grad()
			train_loss = 0.
			outputs = self.model(inputs)
			train_loss = self.criterion(outputs, target)
			# print("agent:", agent_id, "batch:", batch_idx, train_loss)
			train_loss.backward()
			# print("Batch %s update.."%batch_idx)
			# if batch_idx >1:
				# exit()
			self.optimizer.step()
			total_train_loss += train_loss.item()
		epoch_loss = total_train_loss/len(self.train_data_loader)
		self.train_loss_hist.append(epoch_loss)
		if self.kwargs['verbose'] >=2:
			print("epoch_loss", epoch_loss)
		return epoch_loss


	def test(self, agent_id, data_loader = None,  mode ='test'):
		if data_loader is None:
			data_loader = self.test_data_loader

		self.model.eval()
		loss = 0
		correct = 0

		with torch.no_grad():
			for inputs, target in data_loader:
				if self.kwargs['cuda']:
					inputs, target = inputs.cuda(), target.cuda()
				inputs, target = Variable(inputs), Variable(target)
				output = self.model(inputs)
				loss += self.criterion(output, target).item() # sum up batch loss
				pred = output.data.max(1)[1] # get the index of the max log-probability
				correct += pred.eq(target.data.view_as(pred)).long().cpu().sum().item()
				# print(agent_id, loss)

		loss /= len(data_loader)

		if mode == 'test':
			self.test_loss_hist.append(loss)

		elif mode =='train':
			self.global_train_loss_hist.append(loss)
		
		acc = 100. * correct / len(data_loader.dataset)

		if self.kwargs['verbose'] >=2:
			print('Agent {} {} set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
				agent_id, mode, loss, correct, len(data_loader.dataset), acc))
		
		return loss, acc


	def cross_test(self, data_loader):

		self.model.eval()

		avg_loss = 0

		for inputs, target in data_loader:
			if self.kwargs['cuda']:
				inputs, target = inputs.cuda(), target.cuda()

			inputs, target = Variable(inputs), Variable(target)
			# inputs, target = Variable(inputs,requires_grad=True), Variable(target) # test getting gradient wrt input
			# print(inputs.grad)

			self.optimizer.zero_grad()
			output = self.model(inputs)
			loss = self.criterion(output, target)
			# print("Loss: ", loss.item())
			avg_loss += loss.item() # sum up batch loss
			loss.backward()

			try:
				for i in range(len(self.optimizer.param_groups[0]["params"])):
					avg_gradient[i] += self.optimizer.param_groups[0]["params"][i].grad.data
			except:
				avg_gradient = []
				for i in range(len(self.optimizer.param_groups[0]["params"])):

					avg_gradient.append(deepcopy(self.optimizer.param_groups[0]["params"][i].grad.data))

		for i in range(len(self.optimizer.param_groups[0]["params"])): # probably easier if change it to np array?
			avg_gradient[i] /= len(data_loader)
			assert torch.isfinite(avg_gradient[i]).any()
			# print(avg_gradient[i].size())
		
		avg_loss /= len(data_loader)
		
		# for grad in avg_gradient:
			# print("Gradient norm:", torch.norm(grad)) # doesn't work for list of tensor


		print("Avg loss:", avg_loss)

		
		# if self.kwargs['verbose'] >=2:
			# print('Agent {} {} set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
				# agent_id, mode, loss, correct, len(data_loader.dataset), acc))
		
		return avg_gradient, avg_loss


	def log_results(self, epoch):
		log = {}
		log["experiment_name"] = self.experiment_name
		log["agent_id"] = str(self.agent_id)
		log["batch_size"] = self.kwargs['batch_size']
		log["nb_epoch"] = epoch
		log["dataset"] = self.kwargs['dataset']
		log["model_name"] = self.model_name
		log["train_loss"] = self.train_loss_hist
		log["test_loss"] = self.test_loss_hist
		log["global_train_loss"] = self.global_train_loss_hist

		if not os.path.exists(os.path.join("log",self.kwargs['dataset'],self.model_name,self.experiment_name+'_'+str(self.kwargs['omega']))):
			os.makedirs(os.path.join("log",self.kwargs['dataset'],self.model_name,self.experiment_name+'_'+str(self.kwargs['omega'])))
		log_fn = os.path.join("log",self.kwargs['dataset'],self.model_name,self.experiment_name+'_'+str(self.kwargs['omega']),str(self.agent_id)+'.json')
		with open(log_fn, 'w') as fp1:
			json.dump(log, fp1, indent=4, sort_keys=True)


class DistributedAgent(Agent):
	"""docstring for DistributedAgent"""
	def __init__(self, agent_data, test_data, nb_classes, pi = None, agent_id = 1,  **kwargs):
		super(DistributedAgent, self).__init__(agent_data, test_data, nb_classes, agent_id=agent_id, **kwargs)
		self.optimizer.pi = pi
		self.optimizer.n_agents = kwargs['n_agents']
		self.optimizer.agent_id = agent_id
		self.optimizer.omega = kwargs['omega']

	# def get_param_groups(self):
		# return self.optimizer.param_groups

	def get_states(self):
		return self.optimizer.__getstate__()

	def set_states(self, agent_states):
		try:
			self.optimizer.set_agent_states(agent_states)
		except:
			pass

	def set_params_groups(self, agent_param_groups):
		self.optimizer.set_agent_param_groups(agent_param_groups)

	# For CGD
	def set_grad_and_loss(self, gradient, loss):
		self.optimizer.set_data_grad_and_loss(gradient, loss)

	# def set_initial_params_groups(self, std_model_param):
		# self.optimizer.set_start_param_groups(std_model_param)




class Graph(object):
	"""docstring for Graph"""
	def __init__(self, agent_data, train_data, test_data, nb_classes, pi = None, agent_id = 1, **kwargs):
		self.n_agents = kwargs["n_agents"]
		self.agents = []
		self.train_data_loader = data.DataLoader(train_data, kwargs['test_batch_size'])
		self.nb_classes = nb_classes
		self.experiment_name = kwargs['experiment_name']
		self.model_name = kwargs['model_name']
		self.kwargs = kwargs
		self.pi = pi
		self.agent_avg_loss = []
		self.train_avg_loss = []
		self.test_avg_loss = []
		self.train_avg_acc = []
		self.test_avg_acc = []

		# Placeholders:
		self.cross_grad = [[[] for _ in range(self.n_agents)] for _ in range(self.n_agents)] # added
		self.cross_loss = [[[] for _ in range(self.n_agents)] for _ in range(self.n_agents)] # added

		for i in range(self.n_agents):
			self.agents.append(DistributedAgent(agent_data[i], test_data, self.nb_classes, pi = self.pi, agent_id = i, **kwargs))
		self.init_params()

	def share_params(self, epoch):
		agent_state_dicts = [agent.get_states() for agent in self.agents]
		for agent in self.agents:
			agent_states = []
			agent_param_groups = []
			for state_dict in agent_state_dicts:
				if not isinstance(state_dict['state'], defaultdict):
					agent_state = defaultdict(dict)
				else:
					agent_state = state_dict['state']
				agent_states.append(agent_state)
				agent_param_groups.append(state_dict['param_groups'])
			agent.set_states(agent_states)
			agent.set_params_groups(agent_param_groups)

	def share_grad_and_loss(self):
		for i,agent in enumerate(self.agents):
			agent.set_grad_and_loss(self.cross_grad[i], self.cross_loss[i])



	def init_params(self):
		# Init Params was removed because of issue with load_dict
		# In optimizers using buffer, we need default dict
		# load_dict is providing dict and not default dict

		# init_param_group = self.agents[0].get_param_groups()
		for i in range(self.n_agents):
			self.agents[i].model.load_state_dict(self.agents[0].model.state_dict())
			self.agents[i].optimizer.load_state_dict(self.agents[0].optimizer.state_dict())

		self.g_cross_test()
		self.share_grad_and_loss()



		self.share_params(None)

	def gtrain(self, epoch):
		graph_loss = 0
		for agent_id in range(self.n_agents):
			agent_loss = self.agents[agent_id].train(agent_id)
			graph_loss += agent_loss
		graph_loss = graph_loss/self.n_agents
		self.agent_avg_loss.append(graph_loss)
		self.share_params(epoch)
		return graph_loss


	def gtest(self):
		train_loss = 0
		test_loss = 0
		train_acc = 0
		test_acc = 0
		for agent_id in range(self.n_agents):
			loss1, acc1 = self.agents[agent_id].test(agent_id, data_loader = self.train_data_loader, mode ='train')
			train_loss += loss1
			train_acc += acc1
			loss2, acc2 = self.agents[agent_id].test(agent_id)
			test_loss += loss2
			test_acc += acc2

		train_loss /= self.n_agents
		test_loss /= self.n_agents
		train_acc /= self.n_agents
		test_acc /= self.n_agents
		self.train_avg_loss.append(train_loss)
		self.test_avg_loss.append(test_loss)
		self.train_avg_acc.append(train_acc)
		self.test_avg_acc.append(test_acc)

	def g_cross_test(self):
		for agent_id in range(self.n_agents):
			print("===================================")
			print("Agent %s with"%(agent_id))
			for agent_data_num in range(self.n_agents):
				gradient, avg_loss = self.agents[agent_id].cross_test(self.agents[agent_data_num].train_data_loader)
				self.cross_grad[agent_data_num][agent_id] = gradient
				self.cross_loss[agent_data_num][agent_id] = avg_loss


	def log_results(self, epoch):
		log = {}
		log["experiment_name"] = self.experiment_name
		log["batch_size"] = self.kwargs['batch_size']
		log["nb_epoch"] = epoch
		log["dataset"] = self.kwargs['dataset']
		log["model_name"] = self.model_name
		log["agent_avg_loss"] = self.agent_avg_loss
		log["train_avg_loss"] = self.train_avg_loss
		log["test_avg_loss"] = self.test_avg_loss
		log["train_avg_acc"] = self.train_avg_acc
		log["test_avg_acc"] = self.test_avg_acc

		if not os.path.exists(os.path.join("log",self.kwargs['dataset'],self.model_name,self.experiment_name+'_'+str(self.kwargs['omega']))):
			os.makedirs(os.path.join("log",self.kwargs['dataset'],self.model_name,self.experiment_name+'_'+str(self.kwargs['omega'])))
		log_fn = os.path.join("log",self.kwargs['dataset'],self.model_name,self.experiment_name+'_'+str(self.kwargs['omega']),'Graph'+'.json')
		with open(log_fn, 'w') as fp1:
			json.dump(log, fp1, indent=4, sort_keys=True)

		for agent in self.agents:
			agent.log_results(epoch)
