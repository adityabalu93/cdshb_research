**DMSGD Code for NIPS 2018**

This code contains a pytorch implementation of CDSGD, CDMSGD, DMSGD1, DMSGD2 and other algorithms.
For general purpose, this code has a framework to deal with a distributed network of agents and how to perform optimization for each agent written purely in pythonic way.
The classes used are Agent, Distributed Agent and Graph.
Graph is like a module which could be used as graph of graphs if required... because the api for Agent and Graph is same


---

## Requirements

Most of the code is written for python 3.6.0, while it is not supported for other versions, it can still work for them. Specific packages used are

1. Pytorch

---

## Running the code

For running the code:

python main.py -m Model_name -d Dataset -n Number_of_agents -ep epochs -v verbosity -ds data_shuffle -g gpu to use List_of_experiments_nargs 

python main.py -h will provide all the command line arguments possible...

For plotting the graphs:
python eval.py -m model_name -d dataset List_of_experiments_nargs

