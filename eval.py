import os, os.path
import json
import glob
import argparse
import matplotlib.pylab as plt
plt.switch_backend('agg') # ==> "Invalid DISPLAY variable" error pops up without this

def plot_results(list_experiments,dataset,model_name,data_shuffle):

	list_color = [u'#E24A33',
				  u'#348ABD',
				  u'#FBC15E',
				  u'#777777',
				  u'#988ED5',
				  u'#8EBA42',
				  u'#FFB5B8']

	graph_log = []
	agent_log = []

	DIR = os.path.join("log",dataset,model_name,list_experiments[-1],data_shuffle)
	n_agent_log_files = len([name for name in os.listdir(DIR) if os.path.isfile(os.path.join(DIR, name))]) - 1

	for experiment in list_experiments:
		graph_log.append(glob.glob(os.path.join("log",dataset,model_name,experiment,data_shuffle,"Graph.json")))
		for i in range(n_agent_log_files):
			agent_log.append(glob.glob(os.path.join("log",dataset,model_name,experiment,data_shuffle,"%s.json" % (i))))
	# print(graph_log)
	# plotting the graph agent_avg_loss
	plt.figure()
	for idx, log in enumerate(graph_log):
		with open(log[0], "r") as f:
			d = json.load(f)
			color = list_color[idx]
			plt.plot(d["train_avg_loss"],
					 color=color,
					 linewidth=3,
					 label=list_experiments[idx])
			# plt.plot(d["agent_avg_loss"],
			# 		 color=color,
			# 		 linewidth=3,
			# 		 linestyle="--",)
			plt.plot(d["test_avg_loss"],
					 color=color,
					 linewidth=3,
					 linestyle=":",)


	plt.ylabel("Loss", fontsize=20)
	plt.yscale("log")
	plt.xlabel("Number of epochs", fontsize=20)
	plt.title("Graph Average Loss", fontsize=22)
	plt.legend(loc="best")
	plt.tight_layout()
	# plt.savefig("./figures/%s_%s.eps" % (dataset, "local_avg_losses"),format='eps',dpi=1000)
	plt.savefig("./figures/%s_%s.png" % (dataset, "graph_agent_avg_loss"))
	plt.show()

  #  plotting the graph average training accuracies
	plt.figure()
	for idx, log in enumerate(graph_log):
		with open(log[0], "r") as f:
			d = json.load(f)
			color = list_color[idx]
			plt.plot(d["train_avg_acc"],
					 color=color,
					 linewidth=3,
					 label=list_experiments[idx])
			# plt.plot(d["agent_avg_acc"],
			# 		 color=color,
			# 		 linewidth=3,
			# 		 linestyle="--",)
			plt.plot(d["test_avg_acc"],
					 color=color,
					 linewidth=3,
					 linestyle=":",)
	plt.ylabel("accuracy", fontsize=20)
	plt.ylim([0, 100])
	plt.xlabel("Number of epochs", fontsize=20)
	plt.title("Graph average accuracy", fontsize=22)
	plt.legend(loc="best")
	plt.tight_layout()
	# plt.savefig("./figures/%s_%s.eps" % (dataset, "local_avg_accs"),format='eps',dpi=1000)
	plt.savefig("./figures/%s_%s.png" % (dataset, "graph_train_avg_accs"))
	plt.show()






if __name__ == '__main__':


	parser = argparse.ArgumentParser(description='Plot results of experiments')
	parser.add_argument('-d','--dataset', type=str,default="cifar10",
						help='name of the dataset: cifar10, cifar100 or mnist')
	parser.add_argument('list_experiments', type=str, nargs='+',
					help='List of experiment names. E.g. CDSGD EASGD FASGD SGD Adam --> will run a training session with each optimizer')
	parser.add_argument('-m','--model_name', type=str, default="CNN",
					help='Model name')
	parser.add_argument('-ds','--data_shuffle', type=str, default="iid",
					help='data distributed as iid, non-iid, nus or nuc')

	args = parser.parse_args()
	dataset = args.dataset
	list_experiments = args.list_experiments
	model_name = args.model_name
	data_shuffle = args.data_shuffle
	plot_results(list_experiments,dataset,model_name,data_shuffle)
