from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np
import random
from PIL import Image
import torch.utils.data as data
from torchvision import transforms

class Dataset(data.Dataset):
	"""This is an abstract class for a dataset class, to load a given numpy data array
	Args:
	input data
	labels
	train or test
	"""
	def __init__(self, data, labels, train=True,
				 transform=None, target_transform=None,
				 download=False):
		self.transform = transform
		self.target_transform = target_transform
		self.train = train  # training set or test set


		# now load the picked numpy arrays
		if self.train:
			self.train_data = data
			self.train_labels = labels.tolist()
		else:
			self.test_data = data
			# self.test_data = labels.tolist()
			self.test_labels = labels.tolist()

	def __getitem__(self, index):
		"""
		Args:
			index (int): Index
		Returns:
			tuple: (image, target) where target is index of the target class.
		"""
		if self.train:
			img, target = self.train_data[index], self.train_labels[index]
		else:
			img, target = self.test_data[index], self.test_labels[index]

		# doing this so that it is consistent with all other datasets
		# to return a PIL Image
		if self.transform is not None:
			img = self.transform(img)

		if self.target_transform is not None:
			target = self.target_transform(target)

		return img, target

	def __len__(self):
		if self.train:
			return len(self.train_data)
		else:
			return len(self.test_data)



	def __repr__(self):
		fmt_str = 'Dataset ' + self.__class__.__name__ + '\n'
		fmt_str += '    Number of datapoints: {}\n'.format(self.__len__())
		tmp = 'train' if self.train is True else 'test'
		fmt_str += '    Split: {}\n'.format(tmp)
		tmp = '    Transforms (if any): '
		fmt_str += '{0}{1}\n'.format(tmp, self.transform.__repr__().replace('\n', '\n' + ' ' * len(tmp)))
		tmp = '    Target Transforms (if any): '
		fmt_str += '{0}{1}'.format(tmp, self.target_transform.__repr__().replace('\n', '\n' + ' ' * len(tmp)))
		return fmt_str


def create_dataset_class(X_agent_ins, Y_agent_ins):
	datasets = []
	for (x, y) in zip(X_agent_ins, Y_agent_ins):
		dataset = Dataset(x,y,
					transform=transforms.Compose([
					transforms.ToTensor(),
					transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
				    ]))
		datasets.append(dataset)
	return datasets


def make_agent_indices(size, n_agents):
	batch_size = int(np.ceil(size/float(n_agents)))
	return [(i * batch_size, min(size, (i + 1) * batch_size))
			for i in range(0, n_agents)]## in the range of n_agents 


def make_non_uniform_batch_size(size, n_agents):
	nominal_size = int(np.ceil(size/float(n_agents))) ## start from uniform batch size
	start_batch_index = 0
	batch = []
	for i in range(0, n_agents):
		## plus/minus certain percentage from uniform batch 
		## np.random.rand() returns random number from [0,1)
		rand_num = 0.35 * (np.random.rand()-0.5) ## (will have negative number)
		batch_size = int((1 - rand_num) * nominal_size) ## smallest batch size = about 17.5% smaller than uniform batch size (or the remaining data in the end)
		batch.append((start_batch_index, min(size, start_batch_index + batch_size))) ## largest batch = about 17.5% more than uniform batch
		start_batch_index += batch_size
	return batch ## return the index of where the batch should start and end

## array will have attribute 'shape', while list will have attribute 'len'!!
def slice_arrays(arrays, start=None, stop=None):
	if isinstance(arrays, list):## check if 'array' is a 'list'
		if hasattr(start, '__len__'):
			# hdf5 datasets only support list objects as indices
			if hasattr(start, 'shape'):
				start = start.tolist() ## Convert array to 'list'
			return [x[start] for x in arrays]
		else:
			return [x[start:stop] for x in arrays]
	else:
		if hasattr(start, '__len__'):
			if hasattr(start, 'shape'):
				start = start.tolist()
			return arrays[start]
		else:
			return arrays[start:stop]


def load_data(dataset, experiment_category, model_name, n_agents=1, data_shuffle = None):
	# kwargs = {'num_workers': args.n_agents, 'pin_memory': True} if args.cuda else {}## pin_memory = copy data to GPU
	from torchvision import datasets, transforms
	datafile_path = '../data'
	## Load Training Data:
	if dataset == "cifar10":
		train_dataset = datasets.CIFAR10(datafile_path, train=True, download=True,
					transform=transforms.Compose([
					transforms.ToTensor(),
					transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
				    ]))
	
	## Load Testing Data:
		test_dataset = datasets.CIFAR10(datafile_path, train=False,
					transform=transforms.Compose([
					transforms.ToTensor(),
					transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
				    ]))

	elif dataset == "cifar100":
		train_dataset = datasets.CIFAR100(datafile_path, train=True, download=True,
					transform=transforms.Compose([
					transforms.ToTensor(),
					transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
				    ]))

		test_dataset = datasets.CIFAR100(datafile_path, train=False,
					transform=transforms.Compose([
					transforms.ToTensor(),
					transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
				    ]))

	elif dataset == "mnist":
		train_dataset = datasets.MNIST(datafile_path, train=True, download=True,
					   transform=transforms.Compose([
						   transforms.ToTensor(),
						   transforms.Normalize((0.1307,), (0.3081,))
					   ]))

		test_dataset = datasets.MNIST(datafile_path, train=False, transform=transforms.Compose([
						   transforms.ToTensor(),
						   transforms.Normalize((0.1307,), (0.3081,))
					   ]))

	elif dataset == "semeion":
		from semeion import SEMEION
		train_dataset = SEMEION('data', train=True,
					   transform=transforms.Compose([
						   transforms.ToTensor(),
						   transforms.Normalize((0.1307,), (0.3081,))
					   ]))

		test_dataset = SEMEION('data', train=False, transform=transforms.Compose([
						   transforms.ToTensor(),
						   transforms.Normalize((0.1307,), (0.3081,))
					   ]))

	elif dataset == "stl10":
		from stl10 import STL10
		train_dataset = STL10(datafile_path, split='train',
					   transform=transforms.Compose([
						   transforms.ToTensor(),
						   transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
					   ]))

		test_dataset = STL10(datafile_path, split='test', transform=transforms.Compose([
						   transforms.ToTensor(),
						   transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
					   ]))


	## Distribute Data: (Check dims of X and Y Train and Test)
	if experiment_category == "distributed":
		## Split to Training and Testing:
		X_train = train_dataset.data ## numpy array
		Y_train = train_dataset.targets ## convert list to np array
		if dataset == "mnist":
			X_train = X_train.numpy()
			X_train = np.expand_dims(X_train, axis=1)
			print(X_train.shape)
			Y_train = Y_train.numpy()
			# print(type(Y_train[0]))
			# print(Y_train.shape)
			# exit()

		if dataset == 'cifar10' or dataset == 'cifar100' or dataset == 'stl10':
			Y_train = np.asarray(Y_train)		

		if dataset == 'semeion':
			# X_train = X_train.numpy()
			X_train = np.expand_dims(X_train, axis=1)
			print(X_train.shape)
			# print(type(Y_train))
			# print(Y_train.shape)
			# print(type(Y_train[0]))
			# exit()
			# Y_train = Y_train.numpy()
			

		if data_shuffle == "iid":## no shuffling + each agent gets equal number of uniformly distributed data
			nb_classes = len(np.unique(Y_train))
			ins = [X_train, Y_train]
			num_train_samples = len(ins[0]) ## cifar10 = 50000
			index_array = np.arange(num_train_samples) ## returns evenly spaced values (eg: .arange(3) = array([0, 1, 2]))
			np.random.shuffle(index_array)
			agent_batches = make_agent_indices(num_train_samples, n_agents)
			# agent_batches = _make_batches(num_batches, batch_per_agents, n_agents) ## gives batches index instead of each sample index
			## agent_batches --> INDEX of where each batch start and end 
		   
			X_agent_ins = []
			Y_agent_ins = []
			for agent_index, (batch_start, batch_end) in enumerate(agent_batches):
				agent_ids = index_array[batch_start:batch_end]## a range of evenly spaced training sample index
				temp_ins= slice_arrays(ins, agent_ids)
				X_agent_ins.append(temp_ins[0])
				Y_agent_ins.append(temp_ins[1])


		if data_shuffle == "nus":## non-uniform samples size
			nb_classes = len(np.unique(Y_train))
			ins=[X_train,Y_train]
			num_train_samples = len(ins[0])
			index_array = np.arange(num_train_samples)
			agent_batches = make_non_uniform_batch_size(num_train_samples, n_agents)
			
			X_agent_ins = []
			Y_agent_ins = []
			for agent_index, (batch_start, batch_end) in enumerate(agent_batches):
				agent_ids = index_array[batch_start:batch_end]
				temp_ins= slice_arrays(ins, agent_ids)
				X_agent_ins.append(temp_ins[0])
				Y_agent_ins.append(temp_ins[1])

		elif data_shuffle == "nuc":## non-uniform classes (have some information of a class, not not other)
			nb_classes = len(np.unique(Y_train))
			X_train_c = [0 for nb in range(nb_classes)]
			Y_train_c = [0 for nb in range(nb_classes)]
			for select in range(nb_classes):
				indices = np.argwhere(Y_train==select)## return location/index of the element of Y_train = 'select'/counter
				length = indices.shape[0] ## length could be different for different class. (eg: mnist have different data size for different class)
				## Check min and max slice_length
				slice_length = int((0.6*np.random.rand() + 0.4) * length) ## Max length can slice off almost everything in a class????
				# X_temp = X_train[indices[:slice_length, 0], :, :, :].astype('float32')/255. ## Sliced portion of X_train and Y_train (everything in array up to slice_length)
				# Y_temp = Y_train[indices[:slice_length, 0]]
				X_train_c[select] = X_train[indices[:slice_length, 0], :, :, :] ## Store sliced portion
				# Y_train_c[select] = np_utils.to_categorical(Y_temp, nb_classes)
				Y_train_c[select] = Y_train[indices[:slice_length, 0]]
				
				if select == 0:
					X_pool = X_train[indices[slice_length:, 0], :, :, :]#.astype('float32')/255. ## Remaining pool after removing X_temp
					# Y_pool = np_utils.to_categorical(Y_train[indices[slice_length:, 0]],nb_classes)
					Y_pool = Y_train[indices[slice_length:, 0]]

				else:
					## concatenate joins/combines 2 array (like append but it keeps the structure of the original array while append will flatten it)
					X_pool = np.concatenate((X_pool , (X_train[indices[slice_length:, 0], :, :, :])), axis=0)
					# X_pool = np.concatenate((X_pool , (X_train[indices[slice_length:, 0], :, :, :].astype('float32')/255.)) , axis=0)
					# Y_pool = np.concatenate((Y_pool , (np_utils.to_categorical(Y_train[indices[slice_length:, 0]],nb_classes))) , axis=0)
					Y_pool = np.concatenate((Y_pool , (Y_train[indices[slice_length:, 0]])), axis=0)
					## Pools are the remaining array OF EACH CLASS after removing the sliced portions.
					## Took different portion/length from each class


			##Basically the same as non-iid from here on out
			class_per_agent = int(np.ceil(nb_classes/n_agents))

			pool_ins=[X_pool,Y_pool]
			num_pool_samples = pool_ins[0].shape[0]## have .shape now that it is a np array (check the dimension!)
			index_array = np.arange(num_pool_samples)

			pool_batches = _make_non_uniform_batch_size(num_pool_samples, n_agents)
			X_agent_ins = []
			Y_agent_ins = []
			for agent_index, (batch_start, batch_end) in enumerate(pool_batches):
				for select in range(class_per_agent):
					if select==0:
						X_temp = X_train_c[class_per_agent*agent_index+select]
						Y_temp = Y_train_c[class_per_agent*agent_index+select]
					else:
						X_temp = np.concatenate((X_temp, X_train_c[class_per_agent*agent_index+select]), axis=0)
						Y_temp = np.concatenate((Y_temp, Y_train_c[class_per_agent*agent_index+select]), axis=0)
				agent_ids = index_array[batch_start:batch_end]
				temp_ins= _slice_arrays(pool_ins, agent_ids)

				X_temp = np.concatenate((X_temp, temp_ins[0]), axis=0)
				Y_temp = np.concatenate((Y_temp, temp_ins[1]), axis=0)
				print(Y_temp.shape[0])
				X_agent_ins.append(X_temp)
				Y_agent_ins.append(Y_temp)


		elif data_shuffle == "non-iid":## each agent own some classes and don't have some
			nb_classes = len(np.unique(Y_train))
			X_train_c = [0 for nb in range(nb_classes)]
			Y_train_c = [0 for nb in range(nb_classes)]
			for select in range(nb_classes):## for every class?
				indices = np.argwhere(Y_train==select)#? structure of this? 
				# X_temp = X_train[indices[:, 0], :, :, :].astype('float32')/255.
				# Y_temp = Y_train[indices[:, 0]]
				X_train_c[select] = X_train[indices[:, 0], :, :, :]
				Y_train_c[select] = Y_train[indices[:, 0]]

			X_agent_ins = []
			Y_agent_ins = []
			class_per_agent = int(np.ceil(nb_classes/n_agents))
			for nb in range(n_agents): ## classes: 012,123,345 etc(depend on class_per_agent)!
				for select in range(class_per_agent):
					if select==0:
						X_temp = X_train_c[class_per_agent*nb+select]
						Y_temp = Y_train_c[class_per_agent*nb+select]
					else:
						X_temp = np.concatenate((X_temp, X_train_c[class_per_agent*nb+select]), axis=0)
						Y_temp = np.concatenate((Y_temp, Y_train_c[class_per_agent*nb+select]), axis=0)
				X_agent_ins.append(X_temp)
				Y_agent_ins.append(Y_temp)## some classes are not present in some agent's data
		

		datasets = create_dataset_class(X_agent_ins, Y_agent_ins)
		return datasets, train_dataset, test_dataset, nb_classes
		## Note that even if the number of classes per agent is the same, sample size might be different
		## mnist have different sample size in different class

	elif experiment_category == "non-distributed":
		nb_classes = len(np.unique(train_dataset.train_labels))
		return train_dataset, test_dataset, nb_classes