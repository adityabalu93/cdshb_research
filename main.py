from __future__ import print_function

import os
import argparse
import numpy as np
import torch

parser = argparse.ArgumentParser(description='PyTorch Distributed Learning')
parser.add_argument('list_experiments', default='CGD',type=str, nargs='+',
					help='List of experiment names. E.g. SGD MSGD CDSGD ICDSGD ICDMSGD GCDSGD GCDMSGD DMSGD1 DMSGD2 CMSGD1 CMSGD2 CDADAM ICDADAM GCDADAM CDADAMSGD FedAvg CGD MCGD NCDSGD--> will run a training session with each optimizer')
parser.add_argument('-m','--model_name', default='CNN2', type=str,
					help='Model name: CNN, CNN2, Big_CNN, FCN, LR, PreResNet110, WideResNet28x10, stl10_CNN or mnist_CNN')
parser.add_argument('-b','--batch_size', default=512, type=int,
# parser.add_argument('-b','--batch_size', default=64, type=int,
					help='Batch size')
parser.add_argument('-ep','--nb_epoch', default=100, type=int,
					help='Number of epochs')
parser.add_argument('-d','--dataset', type=str, default="cifar10",
					help='Dataset, cifar10, cifar100, mnist, semeion or stl10')
parser.add_argument('-n','--n_agents', default=5, type=int,
					help='Number of agents')
parser.add_argument('-cp','--communication_period', default=1, type=int,
					help='Gap between the communication of the agents')
parser.add_argument('-ds','--data_shuffle', default="non-iid", type=str,
					help='data distributed as iid, non-iid, nus or nuc')
parser.add_argument('-t','--tau', default=5, type=int,
					help='time period for incremental consensus')
parser.add_argument('-g','--gpu', default='0', type=str,
				help='gpu id to use from 0 to 3')
parser.add_argument('-w','--omega', default=0.75, type=float,
				help='omega value of the generalized consensus. For CDADAM, 0.25 = pi_x, 0.5 = pi_all, 0.75 = pi_x,m,v')
parser.add_argument('--test_batch_size', type=int, default=512, metavar='N',
# parser.add_argument('--test_batch_size', type=int, default=64, metavar='N',
					help='input batch size for testing (default: 1000)')
parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
					help='learning rate (default: 0.01)')
parser.add_argument('--momentum', type=float, default=0.95, metavar='M',
					help='Momentum (default: 0.5)')
parser.add_argument('--no-cuda', action='store_true', default=False,
					help='disables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
					help='random seed (default: 1)')
parser.add_argument('--log_interval', type=int, default=10,
					help='how many batches to wait before logging training status')
parser.add_argument('-v','--verbosity', type=int, default=1,
					help='verbosity of the code for debugging, 0==>No outputs, 1==>graph level outputs, 2==>agent level outputs')

args = parser.parse_args()

if args.model_name == "LR":
	if args.dataset != "mnist" and args.dataset != "semeion":
		print("\nOnly mnist or semeion dataset can be run in LR!")
		print("Please choose another model-dataset combination.\n")
		exit()

os.environ["CUDA_VISIBLE_DEVICES"]=args.gpu
args.cuda = not args.no_cuda and torch.cuda.is_available()

torch.manual_seed(args.seed)
if args.cuda:
	torch.cuda.manual_seed(args.seed)

import train 

def launch_training(**kwargs):
	# Launch training
	train.train(**d_params) ## train() function from train.py

if __name__ == "__main__":

	list_dir = ["figures", "log"]
	for d in list_dir:
		if not os.path.exists(d):
			os.makedirs(d)

	for experiment_name in args.list_experiments:
		optimizer = experiment_name.split("_")[0]
		assert optimizer in ["CDSGD", "CDMSGD1", "CDMSGD2", "ICDSGD","ICDMSGD", "GCDSGD", "GCDMSGD", "MSGD", "SGD", "DMSGD1", "DMSGD2", "CDADAM","CDADAMSGD","ICDADAM","GCDADAM","FedAvg","CGD","MCGD", "NCDSGD"], "Invalid optimizer"
		assert args.model_name in ["LR","CNN","CNN2", "Big_CNN", "FCN", "PreResNet110", "WideResNet28x10", "stl10_CNN", "mnist_CNN"], "Invalid model name"
		assert args.dataset in ["cifar10", "cifar100", "mnist", "semeion", "stl10"], "Invalid dataset"
		if optimizer in ["CDSGD", "CDMSGD1", "CDMSGD2", "ICDSGD","ICDMSGD", "GCDSGD", "GCDMSGD", "DMSGD1", "DMSGD2","CDADAM","CDADAMSGD","ICDADAM","GCDADAM","FedAvg","CGD","MCGD", "NCDSGD"]:
			experiment_category="distributed"
			assert args.n_agents > 1
		else:
			experiment_category="non-distributed"
			try:
				assert args.n_agents == 1
			except AssertionError:
				print("In non-distributed optimizer such as",optimizer,
					"you can use only one agent!")
				print("Changing number of agent to 1...")
				args.n_agents = 1


		if experiment_name == "CDADAM":
			if args.omega == 0.25:
				print("Using CDADAM optimizer with pi*x only")
			elif args.omega == 0.5:
				print("Using CDADAM optimizer with pi*all or pi_x,m")
			elif args.omega == 0.75:
				print("Using CDADAM optimizer with pi_x,v,m")

		# Set default params
		d_params = {"experiment_name": experiment_name,
					"batch_size": args.batch_size,
					"nb_epoch": args.nb_epoch,
					"dataset": args.dataset,
					"n_agents": args.n_agents,
					"communication_period":args.communication_period,
					"data_shuffle":args.data_shuffle,
					"experiment_category":experiment_category,
					"tau":args.tau,
					"omega":args.omega,
					"test_batch_size":args.test_batch_size,
					"learning_rate":args.lr,
					"momentum":args.momentum,
					"log_interval":args.log_interval,
					"cuda": args.cuda,
					"model_name": args.model_name,
					"verbose":args.verbosity
					}
		# Launch training
		launch_training(**d_params)