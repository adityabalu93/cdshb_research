import torch
import torch.nn as nn 
import torch.nn.functional as F
import Models


class CNN(nn.Module):

    def __init__(self,ch_dim, fc_nodes, num_classes=10):
        super(CNN, self).__init__()
        self.features = nn.Sequential(
        	nn.Conv2d(ch_dim, 32, 3, padding=1),
			nn.Conv2d(32, 32, 3, padding=1),
			nn.MaxPool2d(2, 2),
			nn.Dropout(p=0.25),
			nn.Conv2d(32, 64, 3, padding=1),
			nn.Conv2d(64, 64, 3, padding=1),
			nn.MaxPool2d(2, 2),
	        )
        self.classifier = nn.Linear(fc_nodes, num_classes)

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        return x

class CNN2(nn.Module):

    def __init__(self,ch_dim, fc_nodes, num_classes=10):
        super(CNN2, self).__init__()
        self.features = nn.Sequential(
        	nn.Conv2d(ch_dim, 32, 3, padding=1),
			nn.Conv2d(32, 32, 3, padding=1),
			nn.MaxPool2d(2, 2),
			nn.Dropout(p=0.25),
			nn.Conv2d(32, 64, 3, padding=1),
			nn.MaxPool2d(2, 2),
			nn.Conv2d(64, 64, 3, padding=1),
			nn.MaxPool2d(2, 2),
	        )
        self.classifier = nn.Linear(fc_nodes, num_classes)

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        return x

# class mnist_CNN(nn.Module):

#     def __init__(self,ch_dim, num_classes=10):
#         super(mnist_CNN, self).__init__()
#         self.features = nn.Sequential(
#         	nn.Conv2d(ch_dim, 32, 3, padding=1),
# 			nn.Conv2d(32, 32, 3, padding=1),
# 			nn.MaxPool2d(2, 2),
# 			nn.Dropout(p=0.25),
# 			nn.Conv2d(32, 64, 3, padding=1),
# 			nn.Conv2d(64, 64, 3, padding=1),
# 			nn.MaxPool2d(2, 2),
# 	        )
#         self.classifier = nn.Linear(3136, num_classes)

#     def forward(self, x):
#         x = self.features(x)
#         x = x.view(x.size(0), -1)
#         x = self.classifier(x)
#         return x


# class stl10_CNN(nn.Module):

#     def __init__(self,ch_dim, num_classes=10):
#         super(stl10_CNN, self).__init__()
#         self.features = nn.Sequential(
#         	nn.Conv2d(ch_dim, 32, 3, padding=1),
# 			nn.Conv2d(32, 32, 3, padding=1),
# 			nn.MaxPool2d(2, 2),
# 			nn.Dropout(p=0.25),
# 			nn.Conv2d(32, 64, 3, padding=1),
# 			nn.Conv2d(64, 64, 3, padding=1),
# 			nn.MaxPool2d(2, 2),
# 	        )
#         self.classifier = nn.Linear(36864, num_classes)

#     def forward(self, x):
#         x = self.features(x)
#         x = x.view(x.size(0), -1)
#         x = self.classifier(x)
#         return x


class Big_CNN(nn.Module):

    def __init__(self,ch_dim, num_classes=10):
        super(Big_CNN, self).__init__()
        self.features = nn.Sequential(
            nn.Conv2d(ch_dim, 64, kernel_size=11, stride=4, padding=5),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.Conv2d(64, 192, kernel_size=5, padding=2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.Conv2d(192, 384, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(384, 256, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, 256, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2),
        )
        self.classifier = nn.Linear(256, num_classes)

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        return x


class FCN(nn.Module):
	def __init__(self, input_dim, nb_classes):
		super(FCN, self).__init__()
		self.input_dim = input_dim
		self.fc1 = nn.Linear(input_dim, 50)
		self.fc2 = nn.Linear(50, 50)
		self.fc3 = nn.Linear(50, nb_classes)

	def forward(self, x):
		x = x.view(-1, self.input_dim)
		x = self.fc1(x)
		for _ in range(20):
			x = self.fc2(x)
		x = self.fc3(x)
		return x


class LR(nn.Module):
	def __init__(self, input_dim, nb_classes):
		super(LR, self).__init__()
		self.input_dim = input_dim
		self.fc1 = nn.Linear(input_dim, nb_classes, bias=False)

	def forward(self, x):
		x = x.view(-1, self.input_dim)
		x = self.fc1(x)
		return x


def load_model(model_name, img_dim, fc_nodes, nb_classes):

	if model_name == "CNN":
		model = CNN(img_dim, fc_nodes, nb_classes)
	elif model_name == "CNN2":
		model = CNN2(img_dim, fc_nodes, nb_classes)
	elif model_name == "Big_CNN":
		model = Big_CNN(img_dim, nb_classes)
	elif model_name == "FCN":
		model = FCN(img_dim, nb_classes)
	elif model_name == "LR":
		model = LR(img_dim, nb_classes)
	elif model_name == "PreResNet110" or model_name == "WideResNet28x10":
		model_cfg = getattr(Models, model_name)
		model = model_cfg.base(*model_cfg.args, num_classes=nb_classes, **model_cfg.kwargs)
	# elif model_name == "WideResNet28x10":
	# 	model_cfg = getattr(Models, 'WideResNet28x10')
	# 	model = model_cfg.base(*model_cfg.args, num_classes=nb_classes, **model_cfg.kwargs)
	# elif model_name == "stl10_CNN":
	# 	model = stl10_CNN(img_dim, nb_classes)
	# elif model_name == "mnist_CNN":
	# 	model = mnist_CNN(img_dim, nb_classes)

	return model, nn.CrossEntropyLoss()