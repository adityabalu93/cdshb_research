import math
import torch
from torch.optim import Optimizer
from collections import defaultdict

import numpy as np

class CDADAM(Optimizer):
	"""Implements Adam algorithm.
	It has been proposed in `Adam: A Method for Stochastic Optimization`_.
	Arguments:
		params (iterable): iterable of parameters to optimize or dicts defining
			parameter groups
		lr (float, optional): learning rate (default: 1e-3)
		betas (Tuple[float, float], optional): coefficients used for computing
			running averages of gradient and its square (default: (0.9, 0.999))
		eps (float, optional): term added to the denominator to improve
			numerical stability (default: 1e-8)
		weight_decay (float, optional): weight decay (L2 penalty) (default: 0)
		amsgrad (boolean, optional): whether to use the AMSGrad variant of this
			algorithm from the paper `On the Convergence of Adam and Beyond`_
	.. _Adam\: A Method for Stochastic Optimization:
		https://arxiv.org/abs/1412.6980
	.. _On the Convergence of Adam and Beyond:
		https://openreview.net/forum?id=ryQu7f-RZ

	self.omega:
	0.25 = pi_x
	0.5 = pi_x,m ==> temporary change to pi_m only
	0.75 = pi_x,m,v

	"""

	def __init__(self, params, lr=1e-3, betas=(0.9, 0.999), eps=1e-8,
				 weight_decay=0, amsgrad=False, momentum=None):
		if not 0.0 <= lr:
			raise ValueError("Invalid learning rate: {}".format(lr))
		if not 0.0 <= eps:
			raise ValueError("Invalid epsilon value: {}".format(eps))
		if not 0.0 <= betas[0] < 1.0:
			raise ValueError("Invalid beta parameter at index 0: {}".format(betas[0]))
		if not 0.0 <= betas[1] < 1.0:
			raise ValueError("Invalid beta parameter at index 1: {}".format(betas[1]))
		defaults = dict(lr=lr, betas=betas, eps=eps,
						weight_decay=weight_decay, amsgrad=amsgrad)
		super(CDADAM, self).__init__(params, defaults)
		# self.old_param_groups = deepcopy(self.param_groups)

	def __setstate__(self, state):
		super(CDADAM, self).__setstate__(state)
		for group in self.param_groups:
			group.setdefault('amsgrad', False)

	def step(self, closure=None):
		"""Performs a single optimization step.
		Arguments:
			closure (callable, optional): A closure that reevaluates the model
				and returns the loss.
		"""
		np.set_printoptions(threshold=np.inf)
		# print("-----------------------------------------")
		# for key in self.agent_states[0].items():
		# 	print(key)
		# print(self.agent_states[0])
		# exit()
		loss = None
		if closure is not None:
			loss = closure()
		if not isinstance(self.state, defaultdict):
			self.state = defaultdict(dict)


		for i, group in enumerate(self.param_groups):
			# print("############################################")
			for j, p in enumerate(group['params']):
				# print("j:",j)
				if p.grad is None:
					continue
				d_p = p.grad.data


				# if grad.is_sparse:
				# 	raise RuntimeError('Adam does not support sparse gradients, please consider SparseAdam instead')
				amsgrad = group['amsgrad']

				param_state = self.state[p]
				# print("=========== self.state ===========")
				# print(self.state)


				# State initialization:
				# if len(param_state) == 0:
				if 'step' not in param_state:
					param_state['step'] = 0
				
				# Exponential moving average of gradient values
				if 'exp_avg' not in param_state:
					param_state['exp_avg'] = torch.zeros(p.data.size()).cuda()

				# Exponential moving average of squared gradient values
				if 'exp_avg_sq' not in param_state:
					param_state['exp_avg_sq'] = torch.zeros(p.data.size()).cuda()

				if amsgrad:
					# Maintains max of all exp. moving avg. of sq. grad. values
					param_state['max_exp_avg_sq'] = torch.zeros(p.data.size()).cuda()

				for k in range(self.n_agents):
					state = self.agent_states[k][self.agent_param_groups[k][i]['params'][j]]
					if 'm_buffer' not in state:
						state['m_buffer'] = torch.zeros(p.data.size()).cuda()
					if 'v_buffer' not in state:
						state['v_buffer'] = torch.zeros(p.data.size()).cuda()


				exp_avg, exp_avg_sq = param_state['exp_avg'], param_state['exp_avg_sq']

				if amsgrad:
					max_exp_avg_sq = param_state['max_exp_avg_sq']
				beta1, beta2 = group['betas']

				param_state['step'] += 1

				# Decay the first and second moment running average coefficient
				exp_avg.mul_(beta1).add_(1 - beta1, d_p) # ==> m_t
				exp_avg_sq.mul_(beta2).addcmul_(1 - beta2, d_p, d_p) # ==> v_t


				# m_con
				if self.omega == 0.5 or self.omega == 0.75:
					m_con = torch.zeros(p.data.size()).cuda()
					m_con.add_(exp_avg).mul_(self.pi[self.agent_id][self.agent_id])
					for k in range(self.n_agents):
						if k != self.agent_id:
							(m_con).add_(self.pi[self.agent_id][k],self.agent_states[k][self.agent_param_groups[k][i]['params'][j]]['m_buffer'])
				else:
					m_con = exp_avg


				# v_con
				if self.omega == 0.75:
					v_con = torch.zeros(p.data.size()).cuda()
					v_con.add_(exp_avg_sq).mul_(self.pi[self.agent_id][self.agent_id])
					for k in range(self.n_agents):
						if k != self.agent_id:
							(v_con).add_(self.pi[self.agent_id][k],self.agent_states[k][self.agent_param_groups[k][i]['params'][j]]['v_buffer'])
				else:
					v_con = exp_avg_sq


				if amsgrad:
					# Maintains the maximum of all 2nd moment running avg. till now
					torch.max(max_exp_avg_sq, v_con, out=max_exp_avg_sq)
					# Use the max. for normalizing running avg. of gradient
					denom = max_exp_avg_sq.sqrt().add_(group['eps'])
				else:
					denom = v_con.sqrt().add_(group['eps']) # ==> v_t_hat


				bias_correction1 = 1 - beta1 ** param_state['step']
				bias_correction2 = 1 - beta2 ** param_state['step']
				step_size = group['lr'] * math.sqrt(bias_correction2) / bias_correction1


				# x_con
				if self.omega != 0.5: # temporarry disable pi_x for CDADAM_0.5
					# print('x-concensus...')
					p.data.mul_(self.pi[self.agent_id][self.agent_id])
					for k in range(self.n_agents):
						if k != self.agent_id:
							(p.data).add_(self.pi[self.agent_id][k], self.agent_param_groups[k][i]['params'][j].data)


				# p.data.addcdiv_(-step_size, exp_avg, denom)
				p.data.addcdiv_(-step_size, m_con, denom)

		return loss


	def set_agent_param_groups(self, agent_param_groups):
		self.agent_param_groups = agent_param_groups

	def set_agent_states(self, states):
		self.agent_states = states

	def set_start_param_groups(self, std_model_param):
		self.param_groups = std_model_param