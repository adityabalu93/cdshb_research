import torch
from torch.optim import Optimizer
from collections import defaultdict

class CDMSGD2(Optimizer):
	def __init__(self, params, lr=0.01, momentum=0.95, dampening=0,
				 weight_decay=0):
		if not 0.0 <= lr:
			raise ValueError("Invalid learning rate: {}".format(lr))
		if not 0.0 <= momentum:
			raise ValueError("Invalid momentum value: {}".format(momentum))
		if not 0.0 <= weight_decay:
			raise ValueError("Invalid weight_decay value: {}".format(weight_decay))

		defaults = dict(lr=lr, momentum=momentum, dampening=dampening,
						weight_decay=weight_decay)
		if (momentum <= 0 or dampening != 0):
			raise ValueError("Nesterov momentum requires a momentum and zero dampening")
		super(CDMSGD2, self).__init__(params, defaults)


	def __setstate__(self, state):
		super(CDMSGD2, self).__setstate__(state)
		for group in self.param_groups:
			group.setdefault('nesterov', True)

	def step(self, closure=None):
		"""Performs a single optimization step.
		Arguments:
			closure (callable, optional): A closure that reevaluates the model
				and returns the loss.
		"""
		loss = None
		if closure is not None:
			loss = closure()
		if not isinstance(self.state, defaultdict):
			self.state = defaultdict(dict)

		for i, group in enumerate(self.param_groups):## Update rule
			weight_decay = group['weight_decay']
			momentum = group['momentum']
			dampening = group['dampening']

			for j, p in enumerate(group['params']):
				if p.grad is None:
					continue
				d_p = p.grad.data

				con_buf = torch.zeros(p.data.size()).cuda()
				con_buf.add_(p.data).mul_(self.pi[self.agent_id][self.agent_id])
				for k in range(self.n_agents):
					if k != self.agent_id:
						(con_buf).add_(self.pi[self.agent_id][k], self.agent_param_groups[k][i]['params'][j].data)

				param_state = self.state[p]
				if 'momentum_buffer' not in param_state:
					buf = param_state['momentum_buffer'] = torch.zeros(p.data.size()).cuda()
					buf.mul_(momentum).add_(d_p)
				else:
					buf = param_state['momentum_buffer']
					buf.mul_(momentum).add_(1 - dampening, d_p)

				d_p.add_(momentum, buf)

				p.data = (con_buf).add_(-group['lr'], d_p)
				# print(torch.norm(d_p), torch.norm(p.data), torch.norm(con_buf))

		return loss

	def set_agent_param_groups(self, agent_param_groups):
		self.agent_param_groups = agent_param_groups