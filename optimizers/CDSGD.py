## Created by: Aditya Balu
import torch
from torch.optim import Optimizer
from collections import defaultdict

class CDSGD(Optimizer):
	def __init__(self, params, lr=0.01, momentum = None):
		if not 0.0 <= lr:
			raise ValueError("Invalid learning rate: {}".format(lr))
		defaults = dict(lr=lr)
		super(CDSGD, self).__init__(params, defaults)


	def __setstate__(self, state):
		super(CDSGD, self).__setstate__(state)

	def step(self, closure=None):
		"""Performs a single optimization step.
		Arguments:
			closure (callable, optional): A closure that reevaluates the model
				and returns the loss.
		"""
		loss = None
		if closure is not None:
			loss = closure()
		if not isinstance(self.state, defaultdict):
			self.state = defaultdict(dict)
		for i, group in enumerate(self.param_groups):## Update rule
			for j, p in enumerate(group['params']):

				if p.grad is None:
					continue
				d_p = p.grad.data
				con_buf = torch.zeros(p.data.size()).cuda()
				con_buf.add_(p.data).mul_(self.pi[self.agent_id][self.agent_id])
				for k in range(self.n_agents):
					if k != self.agent_id:
						(con_buf).add_(self.pi[self.agent_id][k], self.agent_param_groups[k][i]['params'][j].data)
				p.data = (con_buf).add_(-group['lr'], d_p)
				# print(torch.norm(d_p), torch.norm(p.data), torch.norm(con_buf))
		return loss


	def set_agent_param_groups(self, agent_param_groups):
		self.agent_param_groups = agent_param_groups

	def set_start_param_groups(self, std_model_param):
		self.param_groups = std_model_param

	def set_data_grad_and_loss(self, data_gradient, data_loss):
		pass