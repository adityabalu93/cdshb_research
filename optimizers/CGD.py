# Based off DMSGD1
import torch
from torch.optim import Optimizer
from copy import deepcopy
from copy import copy
from collections import defaultdict
import numpy as np
from sklearn.preprocessing import MinMaxScaler
eps=1e-12

class CGD(Optimizer):
	def __init__(self, params, lr=0.01, momentum=0.0, dampening=0,
				 weight_decay=0, nesterov=False, omega=0.5):
		if not 0.0 <= lr:
			raise ValueError("Invalid learning rate: {}".format(lr))
		if not 0.0 <= momentum:
			raise ValueError("Invalid momentum value: {}".format(momentum))
		if not 0.0 <= weight_decay:
			raise ValueError("Invalid weight_decay value: {}".format(weight_decay))
		print('Optimizer initialized with omega value of %s and momentum of %s'%(omega, momentum))
		defaults = dict(lr=lr, momentum=momentum, dampening=dampening,
						weight_decay=weight_decay, nesterov=nesterov, omega=omega)
		if nesterov and (momentum <= 0 or dampening != 0):
			raise ValueError("Nesterov momentum requires a momentum and zero dampening")
		super(CGD, self).__init__(params, defaults)
		self.old_param_groups = deepcopy(self.param_groups)

		self.data_gradient = []
		self.data_loss = []


	def __setstate__(self, state):
		super(CGD, self).__setstate__(state)
		for group in self.param_groups:
			group.setdefault('nesterov', False)

	def step(self, closure=None):
		"""Performs a single optimization step.
		Arguments:
			closure (callable, optional): A closure that reevaluates the model
				and returns the loss.
		"""
		loss = None
		if closure is not None:
			loss = closure()
		if not isinstance(self.state, defaultdict):
			self.state = defaultdict(dict)


		# check data_gradient passing:
		'''
		try:
			print("Agent %s optimizer data gradients: "%self.agent_id)
			print(np.shape(self.data_gradient))
			for grad in self.data_gradient:
				print(torch.norm(grad))
		except:
			print("First epoch")
		'''

		# print("self.param_groups len", len(self.param_groups[0]['params']))
		# print("self.param_groups len", len(self.param_groups))



		for i, group in enumerate(self.param_groups): # always 1
			weight_decay = group['weight_decay']
			momentum = group['momentum']
			dampening = group['dampening']
			nesterov = group['nesterov']
			lr = group['lr']
			# print("###########################################################")

			for j, p in enumerate(group['params']):
				if p.grad is None:
					continue
				d_p = p.grad.data
				# print('norm',torch.norm(p.data))
				assert torch.isfinite(p.data).any()
				assert torch.isfinite(d_p).any()


				# Add x_con, deduct p.data
				temp1 = torch.zeros(p.data.size()).cuda()
				# print("p.data shape:", np.shape(temp))
				temp1 = temp1.add_(p.data)


				if len(self.data_gradient) != 0:
					# p.data.add_(-1.0,temp1)
					# x_con
					con_buf1 = torch.zeros(p.data.size()).cuda()
					sumdxij = torch.zeros(p.data.size()).cuda()
					for k in range(self.n_agents):
						# Multiply with respective gradient:
						dxij = self.data_gradient[k][j]
						sumdxij = sumdxij.add_(dxij)
					assert torch.isfinite(sumdxij).any()
					assert torch.isfinite(con_buf1).any()
					for k in range(self.n_agents):
						# Multiply pi and sum up:
						xij = self.agent_param_groups[k][i]['params'][j].data
						dxij = self.data_gradient[k][j]
						# con_buf1 = con_buf1.add_(torch.mul(xij, 0.2))
						con_buf1 = con_buf1.add_(torch.mul(xij, torch.div(dxij, torch.norm(sumdxij + eps))))
						assert torch.isfinite(con_buf1).any()
					p.data = con_buf1
				else:
					pass

				# deduct lr * gradient (own) ===> third term
				p.data = p.data.add_(-lr, d_p)
				# print('post norm',torch.norm(p.data))

				assert torch.isfinite(p.data).any()
		# print("Remove data_gradient...")
		self.data_gradient = []

		return loss

	def set_agent_param_groups(self, agent_param_groups):
		self.agent_param_groups = agent_param_groups

	def set_agent_states(self, agent_states):
		self.agent_states = agent_states

	def set_data_grad_and_loss(self, data_gradient, data_loss): # self.data_gradient ==> list of tensors, data_loss = list of float
		self.data_gradient = data_gradient
		self.data_loss = data_loss