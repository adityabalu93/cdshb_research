import torch
from torch.optim import Optimizer
from copy import deepcopy
from copy import copy
from collections import defaultdict


class DMSGD1(Optimizer):
	def __init__(self, params, lr=0.01, momentum=0.0, dampening=0,
				 weight_decay=0, nesterov=False, omega=0.5):
		if not 0.0 <= lr:
			raise ValueError("Invalid learning rate: {}".format(lr))
		if not 0.0 <= momentum:
			raise ValueError("Invalid momentum value: {}".format(momentum))
		if not 0.0 <= weight_decay:
			raise ValueError("Invalid weight_decay value: {}".format(weight_decay))
		print('Optimizer initialized with omega value of %s and momentum of %s'%(omega, momentum))
		defaults = dict(lr=lr, momentum=momentum, dampening=dampening,
						weight_decay=weight_decay, nesterov=nesterov, omega=omega)
		if nesterov and (momentum <= 0 or dampening != 0):
			raise ValueError("Nesterov momentum requires a momentum and zero dampening")
		super(DMSGD1, self).__init__(params, defaults)
		self.old_param_groups = deepcopy(self.param_groups)

	def __setstate__(self, state):
		super(DMSGD1, self).__setstate__(state)
		for group in self.param_groups:
			group.setdefault('nesterov', False)

	def step(self, closure=None):
		"""Performs a single optimization step.
		Arguments:
			closure (callable, optional): A closure that reevaluates the model
				and returns the loss.
		"""
		loss = None
		if closure is not None:
			loss = closure()
		if not isinstance(self.state, defaultdict):
			self.state = defaultdict(dict)

		for i, group in enumerate(self.param_groups):
			weight_decay = group['weight_decay']
			momentum = group['momentum']
			dampening = group['dampening']
			nesterov = group['nesterov']
			lr = group['lr']
			# print("###########################################################")

			for j, p in enumerate(group['params']):
				if p.grad is None:
					continue
				d_p = p.grad.data

				param_state = self.state[p]

				# print("=========== self.state ===========")
				# print(self.state)


				# x_con
				con_buf1 = torch.zeros(p.data.size()).cuda()
				con_buf1.add_(p.data).mul_(self.pi[self.agent_id][self.agent_id])
				for k in range(self.n_agents):
					if k != self.agent_id:
						(con_buf1).add_(self.pi[self.agent_id][k], self.agent_param_groups[k][i]['params'][j].data)

				if 'momentum_buffer' not in param_state:
					buf = param_state['momentum_buffer'] = torch.zeros(p.data.size()).cuda()
					for k in range(self.n_agents):
						state = self.agent_states[k][self.agent_param_groups[k][i]['params'][j]]
						if 'momentum_buffer' not in state:
							state['momentum_buffer'] = torch.zeros(p.data.size()).cuda()
				else:
					buf = param_state['momentum_buffer']


				# momentum_con
				con_buf2 = torch.zeros(p.data.size()).cuda()
				con_buf2.add_(buf).mul_(self.pi[self.agent_id][self.agent_id])
				for k in range(self.n_agents):
					if k != self.agent_id:
						(con_buf2).add_(self.pi[self.agent_id][k],self.agent_states[k][self.agent_param_groups[k][i]['params'][j]]['momentum_buffer'])



				# print("-----------------------------------------------------------------")
				# for k in range(self.n_agents):
				# 	print("Agent:",k)
				# 	print(self.agent_states[k][self.agent_param_groups[k][i]['params'][j]]['momentum_buffer'])




				if group['omega'] <= 1.0:
					omega = group['omega']
				else:
					term1_norm = torch.norm(buf)
					term2_norm = torch.norm(con_buf2)
					if term1_norm == 0.0 and term2_norm == 0.0:
						omega = 0.5
					else:
						omega = term1_norm/(term1_norm+term2_norm)

				buf.mul_(momentum*omega)
				buf.add_(con_buf1)
				buf.add_(-1.0,p.data)
				buf.add_(momentum*(1-omega), con_buf2)
				buf.add_(-lr,d_p)

				p.data.add_(buf)

		return loss

	def set_agent_param_groups(self, agent_param_groups):
		self.agent_param_groups = agent_param_groups

	def set_agent_states(self, agent_states):
		self.agent_states = agent_states
