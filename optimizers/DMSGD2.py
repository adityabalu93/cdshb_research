import torch
from torch.optim import Optimizer
from copy import deepcopy
from copy import copy
from collections import defaultdict

class DMSGD2(Optimizer):
	def __init__(self, params, lr=0.01, momentum=0.95, dampening=0,
				 weight_decay=0, nesterov=False):
		if not 0.0 <= lr:
			raise ValueError("Invalid learning rate: {}".format(lr))
		if not 0.0 <= momentum:
			raise ValueError("Invalid momentum value: {}".format(momentum))
		if not 0.0 <= weight_decay:
			raise ValueError("Invalid weight_decay value: {}".format(weight_decay))

		defaults = dict(lr=lr, momentum=momentum, dampening=dampening,
						weight_decay=weight_decay, nesterov=nesterov)
		if nesterov and (momentum <= 0 or dampening != 0):
			raise ValueError("Nesterov momentum requires a momentum and zero dampening")
		super(DMSGD2, self).__init__(params, defaults)
		self.old_param_groups = deepcopy(self.param_groups)

	def __setstate__(self, state):
		super(DMSGD2, self).__setstate__(state)
		for group in self.param_groups:
			group.setdefault('nesterov', False)

	def step(self, closure=None):
		"""Performs a single optimization step.
		Arguments:
			closure (callable, optional): A closure that reevaluates the model
				and returns the loss.
		"""
		loss = None
		if closure is not None:
			loss = closure()
		if not isinstance(self.state, defaultdict):
			self.state = defaultdict(dict)

		for i, group in enumerate(self.param_groups):
			weight_decay = group['weight_decay']
			momentum = group['momentum']
			dampening = group['dampening']
			nesterov = group['nesterov']
			for j, p in enumerate(group['params']):
				if p.grad is None:
					continue
				d_p = p.grad.data

				param_state = self.state[p]

				con_buf = torch.zeros(p.data.size()).cuda()
				con_buf.add_(p.data).mul_(self.pi[self.agent_id][self.agent_id])
				for k in range(self.n_agents):
					if k != self.agent_id:
						(con_buf).add_(self.pi[self.agent_id][k], self.agent_param_groups[k][i]['params'][j].data)

				if 'hb_buffer' not in param_state:
					hb_buf = param_state['hb_buffer'] = torch.zeros(p.data.size()).cuda()
				else:
					hb_buf = param_state['hb_buffer']


				temp_hb_buf = deepcopy(con_buf)

				p.data = con_buf.add_(-group['lr'], d_p).add_(momentum,con_buf.add(-1.0,hb_buf))
				param_state['hb_buffer'] = temp_hb_buf
				try:
					assert not ((temp_hb_buf - hb_buf)==torch.zeros(p.data.size()).cuda()).all()
				except AssertionError:
					print(torch.norm(temp_hb_buf), torch.norm(hb_buf))
					raise AssertionError

		return loss

	def set_agent_param_groups(self, agent_param_groups):
		self.agent_param_groups = agent_param_groups

	# def get_old_params(self):
	# 	self.old_param_groups = deepcopy(self.agent_param_groups)
		# print(len(self.old_param_groups))# == 5
		# print(len(self.old_param_groups[0])) # ==1
		# print(len(self.old_param_groups[0][0])) # ==6
		# print(self.old_param_groups[0][0]["params"])