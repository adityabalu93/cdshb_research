import math
import torch
from torch.optim import Optimizer
from collections import defaultdict


class FedAvg(Optimizer):

	def __init__(self, params, lr=1e-3, betas=(0.9, 0.999), eps=1e-8,
				 weight_decay=0, amsgrad=False, momentum=None):
		if not 0.0 <= lr:
			raise ValueError("Invalid learning rate: {}".format(lr))
		if not 0.0 <= eps:
			raise ValueError("Invalid epsilon value: {}".format(eps))
		if not 0.0 <= betas[0] < 1.0:
			raise ValueError("Invalid beta parameter at index 0: {}".format(betas[0]))
		if not 0.0 <= betas[1] < 1.0:
			raise ValueError("Invalid beta parameter at index 1: {}".format(betas[1]))
		defaults = dict(lr=lr, betas=betas, eps=eps,
						weight_decay=weight_decay, amsgrad=amsgrad)
		super(FedAvg, self).__init__(params, defaults)
		# self.old_param_groups = deepcopy(self.param_groups)

	def __setstate__(self, state):
		super(FedAvg, self).__setstate__(state)
		for group in self.param_groups:
			group.setdefault('amsgrad', False)

	def step(self, closure=None):
		"""Performs a single optimization step.
		Arguments:
			closure (callable, optional): A closure that reevaluates the model
				and returns the loss.
		"""
		loss = None
		if closure is not None:
			loss = closure()
		if not isinstance(self.state, defaultdict):
			self.state = defaultdict(dict)

		for i, group in enumerate(self.param_groups):
			for j, p in enumerate(group['params']):
				if p.grad is None:
					continue
				d_p = p.grad.data

				# if grad.is_sparse:
				# 	raise RuntimeError('Adam does not support sparse gradients, please consider SparseAdam instead')
				amsgrad = group['amsgrad']

				state = self.state[p]

				# State initialization
				if len(state) == 0:
					state['step'] = 0
					# Exponential moving average of gradient values
					state['exp_avg'] = torch.zeros(p.data.size()).cuda()
					# Exponential moving average of squared gradient values
					state['exp_avg_sq'] = torch.zeros(p.data.size()).cuda()
					if amsgrad:
						# Maintains max of all exp. moving avg. of sq. grad. values
						state['max_exp_avg_sq'] = torch.zeros(p.data.size()).cuda()

				exp_avg, exp_avg_sq = state['exp_avg'], state['exp_avg_sq']
				if amsgrad:
					max_exp_avg_sq = state['max_exp_avg_sq']
				beta1, beta2 = group['betas']

				state['step'] += 1

				# Version 1:
				# adam_constant = 1/state['step'] # ==> modify later

				# Version 2: straight line decrease
				if state['step'] <= 100:
					adam_constant = -(1/110) * state['step'] + (111/110) # ==> modify later
				elif state['step'] > 100:
					adam_constant = 0.1

				sgd_constant = 1 - adam_constant # ==> modify later


				# Decay the first and second moment running average coefficient
				exp_avg.mul_(beta1).add_(1 - beta1, d_p)
				exp_avg_sq.mul_(beta2).addcmul_(1 - beta2, d_p, d_p)

				if amsgrad:
					# Maintains the maximum of all 2nd moment running avg. till now
					torch.max(max_exp_avg_sq, exp_avg_sq, out=max_exp_avg_sq)
					# Use the max. for normalizing running avg. of gradient
					denom = max_exp_avg_sq.sqrt().add_(group['eps'])
				else:
					denom = exp_avg_sq.sqrt().add_(group['eps'])

				bias_correction1 = 1 - beta1 ** state['step']
				bias_correction2 = 1 - beta2 ** state['step']
				step_size = group['lr'] * math.sqrt(bias_correction2) / bias_correction1


				# '''
				adam = torch.zeros(p.data.size()).cuda()
				adam.add_(p.data).mul_(self.pi[self.agent_id][self.agent_id])

				for k in range(self.n_agents):
					if k != self.agent_id:
						adam.add_(self.pi[self.agent_id][k], self.agent_param_groups[k][i]['params'][j].data)

				adam.addcdiv_(-step_size, exp_avg, denom)
				# '''


				# sgd:
				# sgd = torch.zeros(p.data.size()).cuda()
				# sgd.add_(p.data).mul_(self.pi[self.agent_id][self.agent_id])
				# for k in range(self.n_agents):
				# 	if k != self.agent_id:
				# 		sgd.add_(self.pi[self.agent_id][k], self.agent_param_groups[k][i]['params'][j].data)
				# sgd.add_(-group['lr'], d_p)

				p.data.mul_(self.pi[self.agent_id][self.agent_id])
				for k in range(self.n_agents):
					if k != self.agent_id:
						(p.data).add_(self.pi[self.agent_id][k], self.agent_param_groups[k][i]['params'][j].data)
				(p.data).add_(-group['lr'], d_p)

				# -----------------------------
				(p.data).mul_(sgd_constant)
				adam.mul_(adam_constant)

				(p.data).add_(adam)

				# Idea:
				# p.data = adam_constant * adam + sgd_constant * sgd

		return loss

	def set_agent_param_groups(self, agent_param_groups):
		self.agent_param_groups = agent_param_groups

	def set_agent_states(self, states):
		self.agent_states = states

	def set_start_param_groups(self, std_model_param):
		self.param_groups = std_model_param