import torch
from torch.optim import Optimizer
from copy import deepcopy

class GCDMSGD(Optimizer):
	def __init__(self, params, lr=0.01, momentum=0, dampening=0,
				 weight_decay=0, nesterov=False):
		if not 0.0 <= lr:
			raise ValueError("Invalid learning rate: {}".format(lr))
		if not 0.0 <= momentum:
			raise ValueError("Invalid momentum value: {}".format(momentum))
		if not 0.0 <= weight_decay:
			raise ValueError("Invalid weight_decay value: {}".format(weight_decay))

		defaults = dict(lr=lr, momentum=momentum, dampening=dampening,
						weight_decay=weight_decay, nesterov=nesterov)
		if nesterov and (momentum <= 0 or dampening != 0):
			raise ValueError("Nesterov momentum requires a momentum and zero dampening")
		super(GCDMSGD, self).__init__(params, defaults)


	def __setstate__(self, state):
		super(GCDMSGD, self).__setstate__(state)
		for group in self.param_groups:
			group.setdefault('nesterov', False)

	def step(self, closure=None):
		"""Performs a single optimization step.
		Arguments:
			closure (callable, optional): A closure that reevaluates the model
				and returns the loss.
		"""
		loss = None
		if closure is not None:
			loss = closure()

		for i, group in enumerate(self.param_groups):## Update rule
			weight_decay = group['weight_decay']
			momentum = group['momentum']
			dampening = group['dampening']
			nesterov = group['nesterov']

			for j, p in enumerate(group['params']):
				if p.grad is None:
					continue
				d_p = p.grad.data ## d_p == g

				# if weight_decay != 0:
					# d_p.add_(weight_decay, p.data)

				if momentum != 0:
					param_state = self.state[p]
					if 'momentum_buffer' not in param_state:
						buf = param_state['momentum_buffer'] = torch.zeros(p.data.size())
						buf.mul_(momentum).add_(d_p)
					else:
						buf = param_state['momentum_buffer']
						buf.mul_(momentum).add_(1 - dampening, d_p)
					if nesterov:
						d_p = d_p.add(momentum, buf)
					else:
						d_p = buf

				# term2 = (p.data.add_(-group['lr'], d_p)).mul_(self.omega)
				term2 = deepcopy(p.data)
				term2.add_(-group['lr'], d_p)
				term2.mul_(self.omega)

				for k in range(self.n_agents):
					if k != self.agent_id:
						(p.data).add_(self.pi[self.agent_id][k], self.agent_param_groups[k][i]['params'][j].data)


				# if weight_decay != 0:
				# 	d_p.add_(weight_decay, p.data)

				
				# Equation:
				# new_p = (1-omega)*d + omega*(p - lr * g) --> new_p = term1 + term2

				
				# term1 = c_p.mul_(1-self.omega)
				(p.data).mul_(1-self.omega)

				# term2 = (p.data.add_(-group['lr'], d_p)).mul_(self.omega) # refer above

				# p.data = (c_p.mul_(1-self.omega)).add_((p.data.add_(-group['lr'], d_p)).mul_(self.omega))
				(p.data).add_(term2)

		return loss

	def set_agent_param_groups(self, agent_param_groups):
		self.agent_param_groups = agent_param_groups