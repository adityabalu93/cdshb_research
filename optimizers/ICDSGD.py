import torch
from torch.optim import Optimizer

class ICDSGD(Optimizer):
	def __init__(self, params, lr=0.01, momentum=0, dampening=0,
				 weight_decay=0, nesterov=False):
		if not 0.0 <= lr:
			raise ValueError("Invalid learning rate: {}".format(lr))
		if not 0.0 <= momentum:
			raise ValueError("Invalid momentum value: {}".format(momentum))
		if not 0.0 <= weight_decay:
			raise ValueError("Invalid weight_decay value: {}".format(weight_decay))

		defaults = dict(lr=lr, momentum=momentum, dampening=dampening,
						weight_decay=weight_decay, nesterov=nesterov)
		if nesterov and (momentum <= 0 or dampening != 0):
			raise ValueError("Nesterov momentum requires a momentum and zero dampening")
		super(ICDSGD, self).__init__(params, defaults)


	def __setstate__(self, state):
		super(ICDSGD, self).__setstate__(state)
		for group in self.param_groups:
			group.setdefault('nesterov', False)

	def step(self, closure=None):
		"""Performs a single optimization step.
		Arguments:
			closure (callable, optional): A closure that reevaluates the model
				and returns the loss.
		"""
		loss = None
		if closure is not None:
			loss = closure()

		for i, group in enumerate(self.param_groups):## Update rule
			weight_decay = group['weight_decay']
			momentum = group['momentum']
			dampening = group['dampening']
			nesterov = group['nesterov']

			for j, p in enumerate(group['params']):
				if p.grad is None:
					continue
				d_p = p.grad.data
				(p.data).mul_(self.pi[self.agent_id][self.agent_id])

				for k in range(self.n_agents):
					if k != self.agent_id:
						(p.data).add_(self.pi[self.agent_id][k], self.agent_param_groups[k][i]['params'][j].data)
				(p.data).add_(-group['lr'], d_p)

		return loss

	def set_agent_param_groups(self, agent_param_groups):
		self.agent_param_groups = agent_param_groups
		
	def set_start_param_groups(self, std_model_param):
		self.param_groups = std_model_param