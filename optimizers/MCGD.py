import torch
from torch.optim import Optimizer
from copy import deepcopy
from copy import copy
from collections import defaultdict
import numpy as np
from sklearn.preprocessing import MinMaxScaler


class MCGD(Optimizer):
	def __init__(self, params, lr=0.01, momentum=0.0, dampening=0,
				 weight_decay=0, nesterov=False, omega=0.5):
		if not 0.0 <= lr:
			raise ValueError("Invalid learning rate: {}".format(lr))
		if not 0.0 <= momentum:
			raise ValueError("Invalid momentum value: {}".format(momentum))
		if not 0.0 <= weight_decay:
			raise ValueError("Invalid weight_decay value: {}".format(weight_decay))
		print('Optimizer initialized with omega value of %s and momentum of %s'%(omega, momentum))
		defaults = dict(lr=lr, momentum=momentum, dampening=dampening,
						weight_decay=weight_decay, nesterov=nesterov, omega=omega)
		if nesterov and (momentum <= 0 or dampening != 0):
			raise ValueError("Nesterov momentum requires a momentum and zero dampening")
		super(MCGD, self).__init__(params, defaults)
		self.old_param_groups = deepcopy(self.param_groups)

		self.data_gradient = []
		self.data_loss = []


	def __setstate__(self, state):
		super(MCGD, self).__setstate__(state)
		for group in self.param_groups:
			group.setdefault('nesterov', False)

	def step(self, closure=None):
		"""Performs a single optimization step.
		Arguments:
			closure (callable, optional): A closure that reevaluates the model
				and returns the loss.
		"""
		loss = None
		if closure is not None:
			loss = closure()
		if not isinstance(self.state, defaultdict):
			self.state = defaultdict(dict)


		# check data_gradient passing:
		'''
		try:
			print("Agent %s optimizer data gradients: "%self.agent_id)
			print(np.shape(self.data_gradient))
			for grad in self.data_gradient:
				print(torch.norm(grad))
		except:
			print("First epoch")
		'''

		# print("self.param_groups len", len(self.param_groups[0]['params']))
		# print("self.param_groups len", len(self.param_groups))



		for i, group in enumerate(self.param_groups): # always 1
			weight_decay = group['weight_decay']
			momentum = group['momentum']
			dampening = group['dampening']
			nesterov = group['nesterov']
			lr = group['lr']
			# print("###########################################################")

			for j, p in enumerate(group['params']):
				if p.grad is None:
					continue
				d_p = p.grad.data

				param_state = self.state[p]

				# x_con
				con_buf1 = torch.zeros(p.data.size()).cuda()
				con_buf1.add_(p.data).mul_(self.pi[self.agent_id][self.agent_id])
				for k in range(self.n_agents):
					if k != self.agent_id:
						(con_buf1).add_(self.pi[self.agent_id][k], self.agent_param_groups[k][i]['params'][j].data)


				# ===============================
				'''
				# momentum_con:
				if 'momentum_buffer' not in param_state:
					buf = param_state['momentum_buffer'] = torch.zeros(p.data.size()).cuda()
					for k in range(self.n_agents):
						state = self.agent_states[k][self.agent_param_groups[k][i]['params'][j]]
						if 'momentum_buffer' not in state:
							state['momentum_buffer'] = torch.zeros(p.data.size()).cuda()
				else:
					buf = param_state['momentum_buffer']


				con_buf2 = torch.zeros(p.data.size()).cuda()
				con_buf2.add_(buf).mul_(self.pi[self.agent_id][self.agent_id])
				for k in range(self.n_agents):
					if k != self.agent_id:
						(con_buf2).add_(self.pi[self.agent_id][k], self.agent_states[k][self.agent_param_groups[k][i]['params'][j]]['momentum_buffer'])


				if group['omega'] <= 1.0:
					omega = group['omega']
				else:
					term1_norm = torch.norm(buf)
					term2_norm = torch.norm(con_buf2)
					if term1_norm == 0.0 and term2_norm == 0.0:
						omega = 0.5
					else:
						omega = term1_norm/(term1_norm+term2_norm)

				buf.mul_(momentum*omega)
				buf.add_(con_buf1)
				buf.add_(-1.0,p.data)
				buf.add_(momentum*(1-omega), con_buf2)
				buf.add_(-lr,d_p)

				p.data.add_(buf)
				'''
				# ===============================


				# # Add x_con, deduct p.data
				# temp = torch.zeros(p.data.size()).cuda()
				# # print("p.data shape:", np.shape(temp))
				# temp.add_(p.data)

				p.data = con_buf1
				# p.data.add_(-1.0,temp)


				# add momentum (not concensus momentum nor momentum sharing)
				if momentum != 0:
					if 'momentum_buffer' not in param_state:
						buf = param_state['momentum_buffer'] = torch.zeros(p.data.size()).cuda()
					else:
						buf = param_state['momentum_buffer']


				# # deduct lr * gradient * momentum (own) ===> third term
				# buf.mul_(momentum).add_(d_p)
				# p.data.add_(-lr, buf)





				# Plus grad_con (need checking)
				if len(self.data_gradient) != 0:

					if self.omega == 0.5: # normalize
						scaler = MinMaxScaler() # default = range 0 to 1
						self.data_loss = np.asarray(self.data_loss).reshape(-1,1)
						scaler.fit(self.data_loss)
						self.data_loss = scaler.transform(self.data_loss).ravel()



					grad_con = torch.zeros(p.data.size()).cuda()
					for k in range(self.n_agents):
						if k != self.agent_id:  # loss_diff is zero anyways for k = self.agent_id
							# print("k:",k)


							# compute loss difference
							# loss_diff = self.data_loss[self.agent_id] - self.data_loss[k] # own loss - other's loss
							loss_diff = self.data_loss[k] - self.data_loss[self.agent_id] # other's loss - own loss
							# loss_diff = (self.data_loss[self.agent_id] - self.data_loss[k]) / abs(self.data_loss[self.agent_id] - self.data_loss[k]) # (own loss - other's loss)/(magnitude) ==> sign only
							# loss_diff = (self.data_loss[k] - self.data_loss[self.agent_id]) / abs(self.data_loss[self.agent_id] - self.data_loss[k]) # (other's loss - own loss)/(magnitude) ==> sign only

							# Multiply with respective gradient:
							temp = torch.zeros(p.data.size()).cuda()
							# temp.add_(self.data_gradient[k])
							# print(temp.size(), p.data.size(), self.data_gradient[k][j].size(), k, j)
							temp.add_(self.data_gradient[k][j])
							# temp.mul_(-loss_diff)

							# Multiply pi and sum up:
							(grad_con).add_(self.pi[self.agent_id][k], temp)
							# print("Summing grad_con...")
							# print(torch.norm(grad_con), loss_diff)
						else:
							(grad_con).add_(self.pi[self.agent_id][k], d_p)

				# Sum grad_con with p.data:
				buf.mul_(momentum).add_(grad_con)
				p.data.add_(-lr, buf)

					# p.data.add_(grad_con)
					# p.data.add_(-lr,grad_con)
					# p.data.add_(lr,grad_con)
					# print(torch.norm(p.data))
					# print(torch.norm(p.data)) # check p.data update
		return loss

	def set_agent_param_groups(self, agent_param_groups):
		self.agent_param_groups = agent_param_groups

	def set_agent_states(self, agent_states):
		self.agent_states = agent_states

	def set_data_grad_and_loss(self, data_gradient, data_loss): # self.data_gradient ==> list of tensors, data_loss = list of float
		self.data_gradient = data_gradient
		self.data_loss = data_loss