## Created by: Aditya Balu
import torch
from torch.optim import Optimizer
from collections import defaultdict

class NCDSGD(Optimizer):
	def __init__(self, params, lr=0.01, momentum = None):
		if not 0.0 <= lr:
			raise ValueError("Invalid learning rate: {}".format(lr))
		defaults = dict(lr=lr)
		super(NCDSGD, self).__init__(params, defaults)


	def __setstate__(self, state):
		super(NCDSGD, self).__setstate__(state)

	def step(self, closure=None):
		"""Performs a single optimization step.
		Arguments:
			closure (callable, optional): A closure that reevaluates the model
				and returns the loss.
		"""
		loss = None
		if closure is not None:
			loss = closure()
		if not isinstance(self.state, defaultdict):
			self.state = defaultdict(dict)
		for i, group in enumerate(self.param_groups):## Update rule
			for j, p in enumerate(group['params']):

				if p.grad is None:
					continue
				d_p = p.grad.data
				#normd = d_p.norm(p=1)
				#print(type(self.agent_param_groups))
				# print (normd)
				# eps = 0.01*normd
				con_buf = torch.zeros(p.data.size()).cuda()
				con_buf.add_(p.data).mul_(self.pi[self.agent_id][self.agent_id])
				
				normd = (p.data).norm(p=2)
				eps = 0.001*normd
				noise = torch.ones(p.data.size())+(torch.randn(p.data.size())*eps)
				noise = noise.cuda()
				con_buf.mul_(noise)
				# if torch.isnan(noise).any()==1:
				# 	print('the noise is too high')
				for k in range(self.n_agents):
					if k != self.agent_id:
						(con_buf).add_(self.pi[self.agent_id][k], (self.agent_param_groups[k][i]['params'][j].data))
						#(con_buf).add_(self.pi[self.agent_id][k], (self.agent_param_groups[k][i]['params'][j].data))
				p.data = (con_buf).add_(-group['lr'], d_p)
				# print(torch.norm(d_p), torch.norm(p.data), torch.norm(con_buf))
		return loss


	def set_agent_param_groups(self, agent_param_groups):
		self.agent_param_groups = agent_param_groups

	def set_start_param_groups(self, std_model_param):
		self.param_groups = std_model_param
	
	def set_data_grad_and_loss(self, data_gradient, data_loss):
		pass