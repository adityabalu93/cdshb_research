import os
import json
import models
import numpy as np
import random
print('GPU:',os.environ['CUDA_VISIBLE_DEVICES'])

import copy
from torchvision import datasets, transforms
import Agent
from handle_data import load_data


def train_non_distributed(**kwargs):
	model_name = kwargs["model_name"]
	mini_batch_size = kwargs["batch_size"]
	test_batch_size = kwargs["test_batch_size"]
	nb_epoch = kwargs["nb_epoch"]
	dataset = kwargs["dataset"]
	experiment_name = kwargs["experiment_name"]
	n_agents = kwargs["n_agents"]
	communication_period = kwargs["communication_period"]
	data_shuffle = kwargs["data_shuffle"]
	experiment_category = kwargs["experiment_category"]
	tau = kwargs["tau"]
	omega = kwargs["omega"]
	learning_rate = kwargs["learning_rate"]
	momentum = kwargs["momentum"]
	log_interval = kwargs["log_interval"]
	verbose = kwargs['verbose']

	train_dataset, test_dataset, nb_classes = load_data(dataset, experiment_category, model_name, n_agents = 1)
	agent = Agent.Agent(train_dataset, test_dataset, nb_classes, **kwargs)

	for epoch in range(1, nb_epoch+1):
		train_loss = agent.train(agent.agent_id)
		test_loss = agent.test(agent.agent_id)
		if epoch%log_interval == 0:
			agent.log_results(epoch)
		if verbose >= 1:
			print("Epoch",epoch, "done with train loss:", train_loss,"\n")


def train_distributed(**kwargs):
	model_name = kwargs["model_name"]
	mini_batch_size = kwargs["batch_size"]
	test_batch_size = kwargs["test_batch_size"]
	nb_epoch = kwargs["nb_epoch"]
	dataset = kwargs["dataset"]
	experiment_name = kwargs["experiment_name"]
	n_agents = kwargs["n_agents"]
	communication_period = kwargs["communication_period"]
	data_shuffle = kwargs["data_shuffle"]
	experiment_category = kwargs["experiment_category"]
	tau = kwargs["tau"]
	omega = kwargs["omega"]
	learning_rate = kwargs["learning_rate"]
	momentum = kwargs["momentum"]
	log_interval = kwargs["log_interval"]
	verbose = kwargs['verbose']

	agent_datasets, train_dataset, test_dataset, nb_classes = load_data(dataset, experiment_category, model_name, data_shuffle = data_shuffle, n_agents=n_agents)

	degreeval=1/n_agents
	pi=degreeval*np.ones((n_agents,n_agents))
	# pi=np.eye(n_agents)
	# pi = np.array([[0.9, 0.1, 0.0, 0.0, 0.0],[0.0, 0.9, 0.1, 0.0, 0.0],
	# 			[0.0, 0.0, 0.9, 0.1, 0.0],[0.0, 0.0, 0.0, 0.9, 0.1],
	# 			[0.1, 0.0, 0.0, 0.0, 0.9]])
	if experiment_name == "ICDSGD" or experiment_name == "ICDMSGD" or experiment_name == "ICDADAM":
		new_pi = np.array(pi)
		for _ in range(tau-1):
			new_pi = np.array(pi).dot(new_pi)
		pi = new_pi
	graph = Agent.Graph(agent_datasets, train_dataset, test_dataset, nb_classes, pi = pi, agent_id = 1, **kwargs)

	for epoch in range(1,nb_epoch+1):
		graph.gtrain(epoch)
		graph.gtest()
		if experiment_name == "CGD" or experiment_name == "MCGD":
			graph.g_cross_test() # added
			graph.share_grad_and_loss()
		
		if epoch%log_interval == 0:
			graph.log_results(epoch)
		if verbose >= 1:
			print("Epoch",epoch, "done with loss:", graph.train_avg_loss[-1],"\n")



def train(**kwargs):
	"""
	Train model

	args: model_name (str, keras model name)
		  **kwargs (dict) keyword arguments that specify the model hyperparameters
	"""
	print('Started running optimizer:',kwargs['experiment_name'],'for',kwargs['nb_epoch'],'epochs')
	if kwargs['experiment_category'] == "non-distributed":
		train_non_distributed(**kwargs)
	elif kwargs['experiment_category'] =="distributed":
		train_distributed(**kwargs)